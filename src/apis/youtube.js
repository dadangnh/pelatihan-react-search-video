import axios from 'axios';

export default axios.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    params: {
        part: 'snippet',
        type: 'video',
        maxResults: process.env.REACT_APP_API_MAX_RESULTS,
        key: process.env.REACT_APP_API_TOKEN,
    }
});
